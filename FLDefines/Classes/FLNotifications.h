//
//  FLNotifications.h
//  FengLei
//
//  Created by liaoxf on 2018/3/18.
//  Copyright © 2018年 com.mlj.FengLei. All rights reserved.
//

#import <Foundation/Foundation.h>

/// 用户被挤下线的通知
UIKIT_EXTERN NSNotificationName const FLUserWasPushedOffTheLineNotification;

/// 没有设置邀请码的通知
UIKIT_EXTERN NSNotificationName const FLDidNotSetInviteCodeNotification;

/// 需要push至预付款用户协议页
UIKIT_EXTERN NSNotificationName const FLNeedsPushToPrepaymentsAgreementVCNotification;

/// 需要push至实名认证
UIKIT_EXTERN NSNotificationName const FLNeedsPushToRenZhengRuKouVCNotification;
