//
//  FLNotifications.m
//  FengLei
//
//  Created by liaoxf on 2018/3/18.
//  Copyright © 2018年 com.mlj.FengLei. All rights reserved.
//

#import "FLNotifications.h"


NSNotificationName const FLUserWasPushedOffTheLineNotification = @"FLUserWasPushedOffTheLineNotification";

NSNotificationName const FLDidNotSetInviteCodeNotification = @"FLDidNotSetInviteCodeNotification";

NSNotificationName const FLNeedsPushToPrepaymentsAgreementVCNotification = @"FLNeedsPushToPrepaymentsAgreementVCNotification";

NSNotificationName const FLNeedsPushToRenZhengRuKouVCNotification = @"FLNeedsPushToRenZhengRuKouVCNotification";
