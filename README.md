# FLDefines

[![CI Status](http://img.shields.io/travis/liao0322/FLDefines.svg?style=flat)](https://travis-ci.org/liao0322/FLDefines)
[![Version](https://img.shields.io/cocoapods/v/FLDefines.svg?style=flat)](http://cocoapods.org/pods/FLDefines)
[![License](https://img.shields.io/cocoapods/l/FLDefines.svg?style=flat)](http://cocoapods.org/pods/FLDefines)
[![Platform](https://img.shields.io/cocoapods/p/FLDefines.svg?style=flat)](http://cocoapods.org/pods/FLDefines)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

FLDefines is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'FLDefines'
```

## Author

liao0322, liao0322@hotmail.com

## License

FLDefines is available under the MIT license. See the LICENSE file for more info.
